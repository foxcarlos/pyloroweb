from django.contrib import admin

from pyloroweb.pyloro.models import *


class SmsEnviadoAdmin(admin.ModelAdmin):
    list_display = ['user', 'texto', 'fecha']
    list_filter = ['fecha', 'user']
    search_fields = ['user__username', 'fecha']

    def queryset(self, request):
        qs = super(SmsEnviadoAdmin, self).queryset(request)
        qs = qs.filter(user=request.user)
        return qs

class SmsBorradorAdmin(admin.ModelAdmin):
    list_display = ['user', 'texto', 'fecha', 'enviado']
    list_filter = ['fecha', 'user', 'enviado']
    search_fields = ['user__username', 'fecha']

class SmsPlanificadoAdmin(admin.ModelAdmin):
    list_display = ['user', 'texto', 'fecha', 'repetir', 'periodo']
    list_filter = ['fecha', 'user']
    search_fields = ['user__username', 'fecha']

admin.site.register(SmsEnviado, SmsEnviadoAdmin)
admin.site.register(SmsBorrador, SmsBorradorAdmin)
admin.site.register(SmsPlanificado, SmsPlanificadoAdmin)