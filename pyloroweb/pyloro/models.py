from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from pyloroweb.agenda.models import Contacto, Lista


class SmsEnviado(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	texto = models.CharField(max_length=200, null=True, blank=True)
	fecha = models.DateTimeField(null=True, blank=True)
	contactos = models.ManyToManyField(Contacto, blank=True)
	listas = models.ManyToManyField(Lista, blank=True)

class SmsBorrador(SmsEnviado):
	enviado = models.BooleanField(default=False)


class SmsPlanificado(SmsEnviado):
	PERIODO_DIARIO, PERIODO_SEMANAL, PERIODO_QUINCENAL, PERIODO_MENSUAL = range(4)
	
	PERIODO = (
        (PERIODO_DIARIO, 'Diario'),
        (PERIODO_SEMANAL, 'Semanal'),
        (PERIODO_QUINCENAL, 'Quincenal'),
        (PERIODO_MENSUAL, 'Mensual'),
    )
	repetir = models.BooleanField(default=False)
	periodo = models.SmallIntegerField(choices=PERIODO, blank=False, null=False)	


