from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, related_name="profile")
    push_notifications = models.BooleanField(default=False)
    email_notifications = models.BooleanField(default=False)
    slack_notifications = models.BooleanField(default=False)


class Plan(models.Model):
    nombre = models.CharField(max_length=140, null=True, blank=True)
    costo = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    cant_usuarios = models.IntegerField(default=0, null=True, blank=True)
    cant_mensajes = models.IntegerField(default=0, null=True, blank=True)

    def __unicode__(self):
        return '{0}'.format(self.nombre)


class Suscripcion(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    plan = models.ForeignKey(Plan, null=True, blank=True)
    fecha = models.DateTimeField(null=True, blank=True)

    """def get_plan(self):
        '''Consultar el plan asociado a un suscriptor (Usuario)'''

		usuario = self.user
	    if not usuario.groups.all().count()
	       print('Usuario {0} no esta asociado a ningun grupo')
	       return False
	    else:
	       nombre_grupo_que_pertenece =  usuario.groups.all()[0].name
	       usuario_padre_del_grupo = User.objects.get(username=nombre_grupo_que_pertenece)
	    
	    
	        usuario = self.user
	        try:
	            plann = Plan.objects.get(id=Suscripcion.objects.get(user=usuario).plan_id)
	        except:
	            plann = None

	        return plann"""