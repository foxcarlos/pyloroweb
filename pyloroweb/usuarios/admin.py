from django.contrib import admin
from pyloroweb.usuarios.models import Profile, Plan, Suscripcion

class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'push_notifications', 'email_notifications']
    list_filter = ['user__username']
    search_fields = ['user__username']

class PlanAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'costo', 'cant_usuarios', 'cant_mensajes']
	list_filter = ['nombre', 'costo']
	search_fields = ['nombre']

class SuscripcionAdmin(admin.ModelAdmin):
	list_display = ['user', 'plan', 'fecha']
	list_filter = ['user__username', 'plan__nombre']
	search_fields = ['user__username', 'plan__nombre']

admin.site.register(Profile, ProfileAdmin)
admin.site.register(Plan, PlanAdmin)
admin.site.register(Suscripcion, SuscripcionAdmin)