from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from pyloroweb.agenda.models import Contacto, Lista
from pyloroweb.agenda.serializers import ContactoSerializer, ListaSerializer


@api_view(['GET', 'POST'])
def contacto_list(request):
	if request.method == 'GET':
		contactos =  Contacto.objects.all()
		serializer = ContactoSerializer(contactos, many=True)
		return Response(serializer.data)

	if request.method == 'POST':
		serializer = ContactoSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def lista_list(request):
	if request.method == 'GET':
		listas =  Lista.objects.all()
		serializer = ListaSerializer(listas, many=True)
		return Response(serializer.data)

@api_view(['GET', 'PUT', 'DELETE'])
def contacto_detalle(request, pk):
	try:
		contacto = Contacto.objects.get(pk=pk)
	except Contacto.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = ContactoSerializer(contacto)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = ContactoSerializer(contacto, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		contacto.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)
