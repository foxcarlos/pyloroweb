from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Contacto(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	nombre =  models.CharField(max_length=200, null=True, blank=True)
	seudonimo = models.CharField(max_length=200, null=True, blank=True)
	apellido = models.CharField(max_length=200, null=True, blank=True)
	telefono = models.CharField(max_length=140, null=True, blank=True)
	telefono2 = models.CharField(max_length=140, null=True, blank=True)
	email = models.CharField(max_length=140, null=True, blank=True)
	email2 = models.CharField(max_length=140, null=True, blank=True)
	fechanac =models.DateTimeField(null=True, blank=True)

	def __unicode__(self):
		return '{0}'.format(self.nombre)

class Lista(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	nombre =  models.CharField(max_length=200, null=True, blank=True)
	contactos = models.ManyToManyField(Contacto, blank=True)

	def __unicode__(self):
		return '{0}'.format(self.nombre)