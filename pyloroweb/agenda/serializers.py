from rest_framework import serializers
from pyloroweb.agenda.models import Contacto, Lista
from django.contrib.auth.models import User


class ContactoSerializer(serializers.ModelSerializer):
	#username = serializers.SerializerMethodField(method_name='get_usuario')

	class Meta:
		model = Contacto
		fields = ('user', 'nombre', 'seudonimo', 'apellido', 'telefono', 'telefono2', 
			'email', 'email2', 'fechanac')

	def get_usuario(self, obj):
		try:
			username = User.objects.get(id=obj.user.id).username
		except:
			username = ''
		return username


class ListaSerializer(serializers.ModelSerializer):

	class Meta:
		model = Lista
		fields = ('user', 'nombre', 'contactos')