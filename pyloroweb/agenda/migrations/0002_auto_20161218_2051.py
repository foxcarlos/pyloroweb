# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-18 20:51
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('agenda', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivatedLista',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visible', models.BooleanField(default=True)),
                ('contacto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agenda.Contacto')),
            ],
        ),
        migrations.RenameModel(
            old_name='Listas',
            new_name='Lista',
        ),
        migrations.AddField(
            model_name='activatedlista',
            name='lista',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agenda.Lista'),
        ),
        migrations.AddField(
            model_name='activatedlista',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
