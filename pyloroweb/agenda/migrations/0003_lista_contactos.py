# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-22 01:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0002_auto_20161218_2051'),
    ]

    operations = [
        migrations.AddField(
            model_name='lista',
            name='contactos',
            field=models.ManyToManyField(blank=True, to='agenda.Contacto'),
        ),
    ]
