from django.contrib import admin
from pyloroweb.agenda.models import *

# Register your models here.

class ContactoAdmin(admin.ModelAdmin):
    list_display = ['user', 'nombre', 'apellido']
    list_filter = ['nombre', 'apellido']
    search_fields = ['user__username', 'nombre']

class ListaAdmin(admin.ModelAdmin):
    list_display = ['user', 'nombre']
    list_filter = ['nombre']
    search_fields = ['user__username', 'nombre']

'''class ActivatedListadmin(admin.ModelAdmin):
    """ Admin de Tag"""
    # change_form_template = 'admin/change_form_customized.html'
    list_display = ['contacto', 'lista', 'user']
    list_filter = ['contacto', 'lista', 'user']
    search_fields = ['contacto__nombre', 'lista__nombre',  'user__username']'''

admin.site.register(Contacto, ContactoAdmin)
admin.site.register(Lista, ListaAdmin)
# admin.site.register(ActivatedLista, ActivatedListadmin)